
## godot-stuff Entity Component System (ECS)

`Release 4.x-R1 (Works with Godot 4.x)`

A Library to provide with Godot with a very simple Entity Component System (ECS) framework.

Official documentation can be found [here.](https://gs-ecs-docs.readthedocs.io/en/latest/)

